/*
 * WebSocketClient.ino
 *
 *  Created on: 24.05.2015
 *
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsClient.h>
#include <Hash.h>

ESP8266WiFiMulti WiFiMulti;
WebSocketsClient webSocket;

#define USE_SERIAL Serial
#define cranePort D1

bool flag = false;

unsigned long disableAt = 0;
unsigned long enabledAt = 0;
int maxWorkTime = 1000 * 60 * 15;
int brakeTime = 1000 * 60 * 5;
bool isBrake = false;

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {

  switch(type) {
    case WStype_DISCONNECTED:
      USE_SERIAL.printf("[WSc] Disconnected!\n");
      break;
    case WStype_CONNECTED: {
      USE_SERIAL.printf("[WSc] Connected to url: %s\n", payload);

      // send message to server when Connected
      sendToServer("connected", "");
    }
      break;
    case WStype_TEXT:
      USE_SERIAL.printf("[WSc] get text: %s\n", payload);
      doCommand(payload);

      // send message to server
      // webSocket.sendTXT("message here");
      break;
    case WStype_BIN:
      USE_SERIAL.printf("[WSc] get binary length: %u\n", length);
      hexdump(payload, length);

      // send data to server
      // webSocket.sendBIN(payload, length);
      break;
        case WStype_PING:
            // pong will be send automatically
            USE_SERIAL.printf("[WSc] get ping\n");
            break;
        case WStype_PONG:
            // answer to a ping we send
            USE_SERIAL.printf("[WSc] get pong\n");
            break;
    }

}

void setup() {
  pinMode(cranePort, OUTPUT);
  closeCrane();
  
  // USE_SERIAL.begin(921600);
  USE_SERIAL.begin(115200);

  //Serial.setDebugOutput(true);
  USE_SERIAL.setDebugOutput(true);

  USE_SERIAL.println();
  USE_SERIAL.println();
  USE_SERIAL.println();

  for(uint8_t t = 4; t > 0; t--) {
    USE_SERIAL.printf("[SETUP] BOOT WAIT %d...\n", t);
    USE_SERIAL.flush();
    delay(1000);
  }

  WiFiMulti.addAP("FineNet", "driga2802");

  //WiFi.disconnect();
  while(WiFiMulti.run() != WL_CONNECTED) {
    delay(100);
  }

  // server address, port and URL
  webSocket.begin("51.15.112.3", 8080, "/");

  // event handler
  webSocket.onEvent(webSocketEvent);

  // try ever 5000 again if connection has failed
  webSocket.setReconnectInterval(5000);
  
  // start heartbeat (optional)
  // ping server every 15000 ms
  // expect pong from server within 3000 ms
  // consider connection disconnected if pong is not received 2 times
  webSocket.enableHeartbeat(15000, 3000, 2);
}


void doCommand(uint8_t * payload) {
  String payload_data = String((char *)payload);
  int event_name_length = payload_data.indexOf(':');
  String event_name = payload_data.substring(0, event_name_length);
  String event_data = payload_data.substring( event_name_length + 1, payload_data.length() );

  USE_SERIAL.printf("event_data: %s", event_data);

  if (event_name == "openCrane") {
    openCrane( event_data.toInt() );
  }

  if (event_name == "closeCrane") {
    closeCrane();
  }

  if (event_name == "getStatus") {
    if (enabledAt > 0) {
      sendToServer("craneStatus", "opened");
    } else {
      sendToServer("craneStatus", "closed");
    }
  }
}

void openCrane(int openingTimeoutSec) {
  USE_SERIAL.printf("openingTimeoutSec: %d", openingTimeoutSec);
  digitalWrite(cranePort, 1);
  enabledAt = millis();
  disableAt = openingTimeoutSec * 1000 + millis();
  sendToServer("craneStatus", "opened");
}

void closeCrane() {
  digitalWrite(cranePort, 0);
  sendToServer("craneStatus", "closed");
}

void sendToServer(String event_name, String value) {
  String data = event_name + ":" + value;
  webSocket.sendTXT( data.c_str() );
}

void loop() {
  if (enabledAt > 0 && millis() > disableAt && !isBrake) {
    closeCrane();
    enabledAt = 0;
    disableAt = 0;
  }

  // enable brake if work more > 15 min
  if (enabledAt > 0 && millis() > enabledAt + maxWorkTime && !isBrake) {
    isBrake = true;
    digitalWrite(cranePort, 0);
  }

  // disable brake if brake already done
  if (enabledAt > 0 && millis() > enabledAt + maxWorkTime + brakeTime && isBrake) {
    digitalWrite(cranePort, 1);
    enabledAt = millis();
    isBrake = false;
    disableAt = disableAt + brakeTime;
  }
  
  webSocket.loop();
}
